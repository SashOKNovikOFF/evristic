function [x, fval, exitflag, output, population, score] = LabWork5()
%% This is an auto generated MATLAB file from Optimization Tool.

%% Start with the default options
options = gaoptimset;
%% Modify options setting
options = gaoptimset(options, 'Generations', 150);
options = gaoptimset(options, 'InitialPopulation', 20);
options = gaoptimset(options, 'Display', 'off');
options = gaoptimset(options, 'PlotFcns', {  @gaplotbestf @gaplotbestindiv @gaplotdistance });
[x, fval, exitflag, output, population, score] = ...
ga(@fitnessFunction, 1, [], [], [], [], 0.0, 10.0, [], [], options);
