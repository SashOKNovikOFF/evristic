#include <vector>
#include <iostream>
#include <cmath>
#include <random>

const int numCities = 6;
const int numAnts = 6;

// ��������� ��������� �����
std::default_random_engine generator;
std::uniform_real_distribution<double> distribution(0.0, 1.0);

int main()
{
	// ������� ���������� ������ �����������
	int map[numCities][numCities] = { {-1, 23, 12, 22, 19, 11},
		                              {23, -1, 10, 14, 26,  8},
		                              {12, 10, -1, 19, 19, 12},
		                              {22, 14, 19, -1, 30, 28},
		                              {19, 26, 19, 30, -1, 28},
								      {11,  8, 12, 28, 28, -1} };
	
	// ���������� �������� �� ������ ����� �����
	double feromon[numCities][numCities] = { {0.5, 0.5, 0.5, 0.5, 0.5, 0.5},
		                                     {0.5, 0.5, 0.5, 0.5, 0.5, 0.5},
		                                     {0.5, 0.5, 0.5, 0.5, 0.5, 0.5},
		                                     {0.5, 0.5, 0.5, 0.5, 0.5, 0.5},
		                                     {0.5, 0.5, 0.5, 0.5, 0.5, 0.5},
		                                     {0.5, 0.5, 0.5, 0.5, 0.5, 0.5} };
	
	// ��������� ������ �� ����������� ���������
	double alpha = 0.3;
	double beta  = 0.7;
	int elite = 1;
	double Q = 90.0;
	double rho = 0.8;
	int timeMax = 10;
	
	// ��������� ���������� ��������
	int coords[numAnts];
	for (int ant = 0; ant < numAnts; ant++)
		coords[ant] = ant;
	
	// ������� ����� ��� ������� �������
	int roads[numAnts][numCities];
	int lengths[numAnts];
	
	// ������� ������� ���� T* � ����� L*
	int bestRoad[numCities];
	int bestLength = 0;
	for (int i = 0; i < numCities - 1; i++)
	{
		bestRoad[i] = i + 1;
		bestLength += map[i][i + 1];
	}
	bestRoad[numCities - 1] = 0;
	bestLength += map[numCities - 1][0];
	
	// ����� ����� ����� ����������� �������
	for (int time = 1; time <= timeMax; time++)
	{
		std::cout << "Time: " << time << std::endl;
		
		for (int ant = 0; ant < numAnts; ant++)
		{
			// �������, ����������, ����� ������ ���� ��������
			bool visited[numCities];
			for (int i = 0; i < numCities; i++)
				visited[i] = false;
			visited[coords[ant]] = true;
			
			int currentCity = coords[ant];
			lengths[ant] = 0;
			
			for (int step = 0; step < numCities - 1; step++)
			{
				std::vector<double> probSquare;
				std::vector<int> probCities;
				
				// ������ ������������
				// "�����������"
				double sum = 0.0;
				for (int y = 0; y < numCities; y++)
					if ((currentCity != y) && !visited[y])
						sum += pow(feromon[currentCity][y], alpha) * pow(1.0 / map[currentCity][y], beta);
				// "���������"
				double chance[numCities];
				int maxInd;
				for (int y = 0; y < numCities; y++)
					if ((currentCity != y) && !visited[y])
					{
						chance[y] = pow(feromon[currentCity][y], alpha) * pow(1.0 / map[currentCity][y], beta) / sum;
						
						probSquare.push_back(chance[y]);
						probCities.push_back(y);
					};
				
				// ����� ����� ����� ��� ������ "�������"
				for (std::size_t y = 1; y < probSquare.size(); y++)
					probSquare[y] += probSquare[y - 1];
				double randNum = distribution(generator);
				for (std::size_t y = 0; y < probSquare.size(); y++)
					if (randNum < probSquare[y])
					{
						maxInd = probCities[y];
						break;
					};
				
				// ���������� ���������� ��������� � ��������� � ��������� �������
				visited[maxInd] = true;
				lengths[ant] += map[currentCity][maxInd];
				roads[ant][step] = maxInd;
				currentCity = maxInd;
			};
			
			// �������� � �������� �������
			roads[ant][numCities - 1] = coords[ant];
			lengths[ant] += map[currentCity][coords[ant]];
		};
		
		// ������� ��������� ������� � ����� ����
		int bestAnt = -1;
		for (int ant = 0; ant < numAnts; ant++)
			if (lengths[ant] < bestLength)
			{
				bestAnt = ant;
				bestLength = lengths[ant];
			};
		
		// ���������� ��������� �������
		if (bestAnt != -1)
			for (int i = 0; i < numCities; i++)
				bestRoad[i] = roads[bestAnt][i];
		
		// ������� �������������� ��������
		double delta[numCities][numCities];
		// �������� ������ �������
		for (int x = 0; x < numCities; x++)
			for (int y = 0; y < numCities; y++)
				delta[x][y] = 0.0;
		// ��������� ������ �������
		for (int ant = 0; ant < numAnts; ant++)
		{
			int xTemp = roads[ant][numCities - 1];
			int yTemp = roads[ant][0];
			delta[xTemp][yTemp] += Q / lengths[ant];
			
			for (int y = 0; y < numCities - 1; y++)
			{
				xTemp = roads[ant][y];
				yTemp = roads[ant][y + 1];
				delta[xTemp][yTemp] += Q / lengths[ant];
			};
		};
		
		// ��������� ���������� ��������� ����
		int xTemp = bestRoad[numCities - 1];
		int yTemp = bestRoad[0];
		delta[xTemp][yTemp] += Q * elite / bestLength;
		
		for (int y = 0; y < numCities - 1; y++)
		{
			xTemp = bestRoad[y];
			yTemp = bestRoad[y + 1];
			delta[xTemp][yTemp] += Q * elite / bestLength;
		};
		
		std::cout << "Length: " << bestLength << std::endl;
		
		// ��������� �������� �� ���� ����� �����
		for (int x = 0; x < numCities; x++)
			for (int y = 0; y < numCities; y++)
				if (x != y)
					feromon[x][y] = (1.0 - rho) * feromon[x][y] + delta[x][y];
	};
	
	std::cout << "Best length: " << bestLength << std::endl;
	std::cout << "Best way: " << std::endl;
	std::cout << bestRoad[numCities - 1] << " ";
	for (int i = 0; i < numCities; i++)
		std::cout << bestRoad[i] + 1 << " ";
	std::cout << std::endl;
	
	return 0;
};