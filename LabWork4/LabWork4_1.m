% 1 �������

p_x = -1:0.01:1;
p_y = -1:0.01:1;
p = [p_x; p_y];
t = [];
for i = 1:201
    t = [t, cos(p_y(i)) * sin(p_x(i))];
end

plot3(p_x, p_y, t, '-k')
hold on