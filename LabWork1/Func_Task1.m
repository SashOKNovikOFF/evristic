function y = Func_Task1(x)
    y = 8 * x - 16 - 12 * (x + 4) .^ (2 / 3);
end