function [x,fval,exitflag,output,population,score] = Task1(nvars,lb,ub,PopulationSize_Data,Generations_Data)
%% This is an auto generated MATLAB file from Optimization Tool.

%% Start with the default options
options = gaoptimset;
%% Modify options setting
options = gaoptimset(options,'PopulationSize', PopulationSize_Data);
options = gaoptimset(options,'Generations', Generations_Data);
options = gaoptimset(options,'Display', 'off');
options = gaoptimset(options,'PlotFcns', {  @gaplotbestf @gaplotbestindiv @gaplotdistance });
[x,fval,exitflag,output,population,score] = ...
ga(@Func_Task1,nvars,[],[],[],[],lb,ub,[],[],options);
