function y = Func_Task2(x)
    y = (x(2) - 3) * exp(- x(1) * x(1) - x(2) * x(2));
end